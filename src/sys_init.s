 
.title "_sys_init: System Hardware Initialization"
.include "neoteric.s"

.global _sys_init

.text
_sys_init:
	// Set up the screen
	pr_str	$_TERM_HOME_CURSOR
	pr_str	$_TERM_CLEAR_SCREEN
	pr_strl	$banner
	pr_crlf

	// TODO: Set up memory layout

	// TODO: Start memory allocation pools
	call	memory_setup_primary_heap 2 $4096 $010000

	bis	$1, _system_shutdown

	// Start the lineclock
	jsr	pc, lineclock_start

	rts	pc

.data

your_number: .string "Your number is: "
banner: .string "……… ✇ NeotericOS 0.1 ………………………………………………………………………………"

.end                      

