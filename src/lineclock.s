
.title "lineclock: start interrupts on linelock, optional special handler in R1"
.include "neoteric.s"

.global lineclock_start
.global lineclock_start_custom
.global lineclock_stop
.global lineclock_get_update
.global lineclock_clear_update
.global lineclock_get_ticks
.global lineclock_get_seconds
.global lineclock_get_minutes
.global lineclock_get_hours
.global lineclock_get_days


LINECLOCK_ADDR = 0177546    // permanent address of KW-11 lineclock
LINECLOCK_VEC_PC = 0100	    // permanent vector for lineclock, new address
LINECLOCK_VEC_PS = 0102	    // permanent vector for lineclock, new PS
LINECLOCK_PS = 0b0000000011100000 // kernel mode, priority 7, no flags


.text

lineclock_start:
	mov	$default_handler, LINECLOCK_VEC_PC  // use default handler
	jsr	pc, enable_interrupt
	rts	pc

lineclock_start_custom:
	param	0, LINECLOCK_VEC_PS	    // use custom interrupt handler
	jsr	pc, enable_interrupt
	rts	pc

lineclock_clear_update:
	clr	update
	rts	pc

lineclock_stop:
	clr	LINECLOCK_ADDR
        rts     pc

.macro get var
lineclock_get_\var:
	mov	\var, r1
	rts	pc
.endm
get update
get ticks
get seconds
get minutes
get hours
get days

enable_interrupt:
	mov	LINECLOCK_PS, LINECLOCK_VEC_PS	    // set up new psw in vector
	mov	$0100, LINECLOCK_ADDR		    // bit 6 = interrupt enable 
        rts     pc

default_handler:
tick:	inc	ticks
	cmp	ticks, $60
	bge	second
	rti
second: inc	seconds
	clr	ticks
	inc	update
	cmp	seconds, $60
	bge	minute
	rti
minute: inc	minutes
	clr	seconds
	cmp	minutes, $60
	bge	hour
	rti
hour:	inc	hours
	clr	minutes
	cmp	hours, $24
	bge	day
	rti
day:	inc	days
	clr	hours
	rti

.data

update:	    .word 0
ticks:	    .word 0
seconds:    .word 0
minutes:    .word 0
hours:	    .word 0
days:	    .word 0

.end

