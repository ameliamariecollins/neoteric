
.title "VT100+ Terminal Control Codes"
.include "neoteric.s"

.text

.macro pr_vt100 name string
    .global vt100_\name
    vt100_\name:
	    pr_str	$TERM_VT100_\string
	    rts	pc
.endm

pr_vt100 bell BELL
pr_vt100 bs BS
pr_vt100 ht HT
pr_vt100 lf LF
pr_vt100 cr CR
pr_vt100 esc ESC
pr_vt100 csi CSI
pr_vt100 ris RIS
pr_vt100 decsc DECSC
pr_vt100 decrc DECRC
pr_vt100 decpnm DECPNM
pr_vt100 decpam DECPAM

.macro pr_vt100_color attr1 attrs:vararg
    pr_vt100_csi
    .irp attr \attrs
	pr_char	$59
	pr_uint	0770 $\attr
    .endr
	pr_char	$109
.endm

// CSI (CONTROL SEQUENCE INTRODUCER, ESC [) COLOR SEQUENCES
// CSI N m	SGR	ANSI color code (Select Graphic Rendition)
// CSI 38 ; 5 ; n m	256COLOR	Foreground 256 color code
// ESC 48 ; 5 ; n m	256COLOR	Background 256 color code
// CSI 38 ; 2 ; r ; g ; b m	TRUECOLOR	Foreground 24 bit rgb color code
// ESC 48 ; 2 ; r ; g ; b m	TRUECOLOR	Background 24 bit rgb color code

// ECMA-48 CSI SEQUENCES
// Code	Effect	Note
// CSI n @	ICH	Insert n blank characters
// CSI n A	CUU	Move cursor up n rows
// CSI n B	CUD	Move cursor down n rows
// CSI n C	CUF	Move cursor forward n columns
// CSI n D	CUB	Move cursor backward n columns
// CSI n E	CNL	Move cursor down n rows, to column 1
// CSI n F	CPL	Move cursor up n rows, to column 1
// CSI n G	CHA	Move cursor to column n
// CSI n ; m H	CUP	Move cursor to row n, and column m
// CSI n J	ED	0 erase from cursor to end of display, 1 erase from start of display to cursor, 2 erase display
// CSI n K	EL	0 erase from cursor to end of line, 1 erase from start of line to cursor, 2 erase line
// CSI n L	IL	Insert n blank lines
// CSI n M	DL	Delete n lines
// CSI n P	DCH	Delete n characters
// CSI n X	ECH	Erase n characters
// CSI n ; m r	DECSTBM	Set scrolling region between top row n and bottom row m

.data

.macro gen_term_data name string
    TERM_\name: .string string
.endm

// CONTROL CHARACTERS
gen_term_data VT100_BELL	"\x07"	    // ring the terminal bell
gen_term_data VT100_BS		"\x08"	    // backspace
gen_term_data VT100_HT		"\x09"	    // tab
gen_term_data VT100_LF		"\x0a"	    // linefeed
gen_term_data VT100_CR		"\x0b"	    // carriage return
gen_term_data VT100_ESC		"\x1b"	    // escape
gen_term_data VT100_CSI		"\x1b["	    // escape

// ESCAPE SEQUENCES
gen_term_data VT100_RIS		"\x1bc"	    // reset terminal to initial state
gen_term_data VT100_DECSC   	"\x1b7"	    // save cursor position/attributes
gen_term_data VT100_DECRC	"\x1b8"	    // restore cursor position/attributes
gen_term_data VT100_DECPNM	"\x1b>"	    // set numeric keypad mode
gen_term_data VT100_DECPAM	"\x1b="	    // set applicaion keypad mode

gen_term_data HOME_CURSOR	"\x1b[H"
gen_term_data CLEAR_SCREEN	"\x1b[2J"

.end                      

