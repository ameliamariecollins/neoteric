.title "Data to string conversion"
.include "neoteric.s"

.global format_uint_dec
.global format_uint_oct
.global format_uint_hex

.text

format_uint_dec:
	pushbp
	push	r0	    // save working registers
	push	r1
	push	r2
	push	r3
	push	r4

	param	0, r1	    // dividend
	mov	$10000, r3  // units place
	param	1, r4	    // output string address
	clr	r5	    // flag: past leading zeros
    0$:
	clr	r0
	div	r3, r0	    // divide by units place
  	bne	1$	    // quotient is non-zero? add digit
	cmp	$1, r3	    // single units position?
	beq	1$	    // ...always append to string
	tst	r5	    // leading zero?
	beq	2$	    // ...skip
    1$:
        bis	$1, r5	    // no more leading zeros
	add	$060, r0    // convert quotient to ASCII digit
 	movb	r0, (r4)+   // append to string
    2$:
	clr	r2	    // clear units place quotient
 	div	$10, r2	    // reduce units place
	mov	r2, r3	    // move quotient back to r3
	bne	0$	    // not at single units position? continue
	movb	$0, (r4)+   // append null byte

	pop	r4	    // restore working registers
	pop	r3
	pop	r2
	pop	r1
	pop	r0
	popbp
	rts	pc


format_uint_oct:
	pushbp
	push	r0	    // save working registers
	push	r1
	push	r2
	push	r3
	push	r4

	param	0, r1	    // dividend
	mov	$32768, r3  // units place
	param	1, r4	    // output string address
	clr	r5	    // flag: past leading zeros

	movb	$48, (r4)+  // single leading zero always
    0$:
	clr	r0
	div	r3, r0	    // divide by units place
  	bne	1$	    // quotient is non-zero? add digit
	tst	r5	    // leading zero?
	beq	2$	    // ...skip
    1$:
        bis	$1, r5	    // no more leading zeros
	add	$060, r0    // convert quotient to ASCII digit
 	movb	r0, (r4)+   // append to string
    2$:
	clr	r2	    // clear units place quotient
 	div	$8, r2	    // reduce units place
	mov	r2, r3	    // move quotient back to r3
	bne	0$	    // not at single units position? continue
	movb	$0, (r4)+   // append null byte

	pop	r4	    // restore working registers
	pop	r3
	pop	r2
	pop	r1
	pop	r0
	popbp
	rts	pc


format_uint_hex:
	pushbp
	push	r0	    // save working registers
	push	r1
	push	r2
	push	r3
	push	r4

	param	0, r1	    // dividend
	mov	$4096, r3   // units place
	param	1, r4	    // output string address
	clr	r5	    // flag: past leading zeros

	movb	$48, (r4)+  // 0
	movb	$120, (r4)+  // x
    0$:
	clr	r0
	div	r3, r0	    // divide by units place
  	bne	1$	    // quotient is non-zero? add digit
	cmp	$1, r3	    // single units position?
	beq	1$	    // ...always append to string
	tst	r5	    // leading zero?
	beq	2$	    // ...skip
    1$:
        bis	$1, r5	    // no more leading zeros
	add	$48, r0	    // convert quotient to ASCII digit
	cmp	r0, $57
	ble	3$
	add	$39, r0
    3$:
 	movb	r0, (r4)+   // append to string
    2$:
	clr	r2	    // clear units place quotient
 	div	$16, r2	    // reduce units place
	mov	r2, r3	    // move quotient back to r3
	bne	0$	    // not at single units position? continue
	movb	$0, (r4)+   // append null byte

	pop	r4	    // restore working registers
	pop	r3
	pop	r2
	pop	r1
	pop	r0
	popbp
	rts	pc

.end
