
================================
THE AUBERGINE CALLING CONVENTION
================================
-------------------------------------------------
*Amelia Marie Collins* <amelia@appliedneural.net>
-------------------------------------------------

The Aubergine calling convention is the first such convention
implemented for the Neoteric operating system for the PDP-11/70.  The
"color" of calling convention is not a versioning label; other colors,
when and if they exist, are different, possibly incompatible schemes,
and colors of calling convention schemes may or may not correspond to
colors of other system schemes.

Aubergine is based on the following notions and assumptions:

 * Stacks grow *downward*,

 * R5 is sacrificed as the "base pointer" for parameter passing,
   and must be saved/restored by routines who themselves
   make a call using this convention,

 * callers are responsible for providing space for routines
   to store their returned parameters, and

 * each item on the stack is a single word (but can be the address
   of an allocated data structure).

Aubergine is parameterized to provide for stack pointers other than
SP/R6. Convenience macros are provided to provide a consistent
interface and to make macro insertion appear as much like a native
instruction call as possible, including attention to parameter
ordering. Sensible defaults are provided to reduce code bulk and
visual noise.

THE BASE POINTER
----------------

The R5 register is used as a pointer to the first in-bound parameter on
the stack. Since the caller is required to push inbound parameters (if
any are to be passed) in reverse order, and because the stack grows
downward, R5 therefore holds the address of the first parameter
(parameter zero). Additional parameters, if any, reside at ascending
addresses, in order. Thus, parameter 2 can be found at (2*2)(BP).
Likewise, space for values to be returned by the routine are BELOW this
address, in reverse order so that returned value zero is at -2(BP),
value one as at -4(BP) and so on. Macros are provided that include this
address calculation so that routines can simply refer to "param 0" or
"rval 3."

CALLING A SUBROUTINE WITH AUBERGINE
-----------------------------------

To make a subroutine call using Aubergine (or to call any routine that
may also use this calling convention), the caller must save the
current base pointer -- and restore it before returning to *its*
caller. In the normal case, this can be done with the macros
**pushbp** and **popbp**, which push BP/R5 onto the stack pointed to
by SP (or a specified stack pointer).

Before the call is to be made, the stack frame must be created. First,
if the subroutine accepts input parameters, they must be pushed onto
the stack in reverse order. That is, if there are three parameters,
the third must be pushed, the second must then be pushed, and finally
the first. This can be done using the normal method of stack
deposition on the PDP-11, **move val, -(sp)**. If no paramaeters are
to be passed, this step can be omitted.

Whether or not parameters are pushed onto the stack, the address
pointed to be the stack pointer should be transferred to BP/R5 as the
base pointer value for the call. This can be accomplished by calling
the **setbp** macro.

Once this is done, space can be allocated on the stack for return
values using the **rvalues** macro, specifying the number of values
that the subroutine will return. Additionally, arbitrary space can be
reserved on the stack for return values, temporary space, and so on.
This is done by increasing the number supplied to **rvalues** (each
increment resrves a word). This macro can be called multiple times to
reserve even more space. If no values are to be returned and no stack
work space is needed, this macro need not be called.

The caller may JSR to the subroutine using the PC or R0-4 registers
as required.

--------------------------
DURING THE SUBROUTINE CALL
--------------------------

Subroutine extracts params: param N is at N*2(R5).
Subroutine writes return values: rval N is (N+1)*-2(R5).
Subroutine returns, leaving R5 intact.

----------------------------
AFTER THE SUBROUTINE RETURNS
----------------------------

Caller extracts return values.
Caller discards stack frame.
Caller restores R5.

--------------------------
EXAMPLE OF SUBROUTINE CALL
--------------------------

