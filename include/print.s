
.macro	pr_char	char
	jsr     pc, con_txrdy
	pushb	\char
	setbp
	jsr	pc, con_putch
	dparam	1
.endm

.macro	pr_crlf
	pr_char	$13
	pr_char	$10
.endm

.macro	pr_str	addr
	call	con_putcstr 1 \addr
.endm

.macro	pr_strl addr
	pr_str	\addr
	pr_crlf
.endm

.macro	uintstr addr uint base=dec
	call	format_uint_\base 2 \addr \uint
.endm

.macro	pr_uint_dec addr uint base=dec
	uintstr	\addr \uint \base
	pr_str	\addr
.endm

.macro	pr_set items:vararg
    .irp item \items
	pr_str \item
    .endr
.endm

.macro	pr_setl items:vararg
    pr_set \items
    pr_crlf
.endm

