//// STACK MANIPULATION MACROS

.macro	push	src=r0 ptr=sp
        mov	\src, -(\ptr)
.endm

.macro	pushb	src=r0 ptr=sp
        movb	\src, -(\ptr)
.endm

.macro	pop	dst=r0 ptr=sp
        mov	(\ptr)+, \dst
.endm

.macro	popb	dst=r0 ptr=sp
        movb	(\ptr)+, \dst
.endm


