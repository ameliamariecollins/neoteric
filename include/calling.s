//// AUBERGINE CALLING CONVENTION

// See aubergine-calling-convention.rst information and examples.

//--- CALLER, PRE-CALL

.macro	setbp	src=sp
	mov	\src, r5
.endm

// rvalues: provide space on the stack for subroutine to return values
.macro	rvalues num=1 ptr=sp
	sub	$\num*2, \ptr
.endm

//--- THE CALL
//    Note: params must be in reverse order.

.macro	call	func, num_param, params:vararg
    .irp param \params
	push \param
    .endr
	setbp
	jsr	pc, \func
	dparam	\num_param
.endm

//--- CALLEE

.macro	pushbp	dst=sp
	mov	r5, -(\dst)
.endm

.macro	popbp	src=sp
	mov	(\src)+, r5
.endm

// param: Fetch input parameters from stack frame (used by subroutines)
.macro	param	select=0 dst=r0
	mov	2*\select(r5), \dst
.endm

.macro	paramb	select=0 dst=r0
	movb	2*\select(r5), \dst
.endm

// rval: Place return values into caller-provided stack positions
//	 (used by subroutines)
.macro	rval	src=r0 select=0
	mov	\src, -2*\select(r5)
.endm

.macro	rvalb	src=r0 select=0
	movb	\src, -2*\select(r5)
.endm

//--- CALLER, POST-CALL

// getrv: fetch values rerurned to stack by subroutine
//        (used by caller after subroutine returns)
.macro	getrv	select=0 dst=r0
	mov	-2*\select(r5), \dst
.endm

.macro	getrvb	select=0 dst=r0
	movb	-2*\select(r5), \dst
.endm

// dparam: Discard stack parameters after subroutine call (used by callers)
.macro	dparam	num=1 ptr=sp
	add	$\num*2, r5
	mov	r5, \ptr
.endm

