.title	 "Memory Manipulationa and Allocation"
.ident	 "V01.00"
.include "neoteric.s"

.global memory_setup_primary_heap
.global memory_fill
.global memory_fillb
.global memory_size_rank

.text

memory_setup_primary_heap:
	pushbp
	push	r2
	push	r3
	push	r4

	param   0, r3	    // address of memory area
	param   1, r4	    // size  ''  ''  ''
	mov	r3, r2	    // memory usage counter

	// Print allocation to console
	uintstr	$0700 r4 dec
	uintstr	$0710 r3 oct
 	pr_setl	$memory_desc1 $0700 $memory_desc2 $0710 $memory_desc3

	// Zero-fill area
	call	memory_fill 3 $0 r4 r3

	/* Initial free-list allocations for a 4KiB region and its data structures:
	.---
	|   0: Rank  0: 0
	|   1: Rank  1: 0
	|   2: Rank  2: 0
	|   3: Rank  3: 0
	|   4: Rank  4: 0
	|   5: Rank  5: 0
	|   6: Rank  6: 42
	|   7: Rank  7: 38
	|   8: Rank  8: 34
	|   9: Rank  9: 30
	|  10: Rank 10: 26
	|  11: Rank 11: 22
	|  12: Rank 12: 0
	|  13: Rank 13: 0
	|  14: Rank 14: 0
	|  15: Rank 15: 0
	|  16: Rank 16: 0
	|  17: Rank 17: 0
	|  18: Rank 18: 0
	|  19: Rank 19: 0
	|  20: Rank 20: 0
	|  21: Rank 21: 0
	|  22: Free list entry: 2048-word buddy: 0, 2048
	|  26: Free list entry: 1024-word buddy: 0, 1024
	|  30: Free list entry:  512-word buddy: 0,  512
	|  34: Free list entry:  256-word buddy: 0,  256
	|  38: Free list entry:  128-word buddy: 0,  128
	|  42: Free list entry:   64-word buddy: 0,   64
	`---

	First 32-word block contains 21 words of free list heads.
	Second 32-word block contains 12 words of 2-word free list entries.
	These regions of memory can be added to their respective free lists:
	4 words at 44, and 16 words at 48.
	*/

 	mov $42, 6(r3)
  	mov $38, 8(r3)
 	mov $34, 10(r3)
 	mov $30, 12(r3)
 	mov $26, 14(r3)
 	mov $22, 16(r3)
 
 	mov $2048, 24(r3)
 	mov $1024, 28(r3)
 	mov  $512, 32(r3)
 	mov  $256, 36(r3)
 	mov  $128, 40(r3)
 	mov   $64, 44(r3)

	pop	r4
	pop	r3
	pop	r2
	popbp
	rts	pc

memory_size_rank:
	push	r0	    // save registers
	push	r1
	param   0, r1	    // fetch size from stack params

	tst	r1	    // size can't be zero
	bne	1$
	clr	r0	    // return zero, which is OOB for rank
	br	9$

    1$:	mov	$15, r0	    // leftmost bit position counter
    2$:	asl	r1	    // find leftmost bit
	bcs	3$
	dec	r0
	br	2$
    3$: tst	r1	    // any remainder?
	beq	9$
	inc	r0	    // ...increase to next rank to contain data

    9$: 
	rval
	pop	r1
	pop	r0
	rts	pc


memory_fill:
	push	r0		// save registers
	push	r1
	push	r2
	param   0, r0		// fetch starting address from stack params
	param   1, r1		// fetch size (in words)  ''  ''  ''
	param   2, r2		// value (word) to write
	
    0$:	mov	r2, (r0)+	// write value to all words in range
 	sob	r1, 0$

	pop	r2		// restore registers
	pop	r1
	pop	r0
	rts	pc


memory_fillb:
	push	r0		// save registers
	push	r1
	push	r2
	param   0, r0		// fetch starting address from stack params
	param   1, r1		// fetch size (in bytes)  ''  ''  ''
	paramb  2, r2		// value (byte) to write

    0$:	movb	r2, (r0)+	// write value to all bytes in range
 	sob	r1, 0$

	pop	r2		// restore registers
	pop	r1
	pop	r0
	rts	pc

.data

memory_desc1: .string "Now allocating "
memory_desc2: .string " bytes at address "
memory_desc3: .string "."
rank_desc1: .string "The rank was "

.end
