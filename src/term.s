 
.title "Terminal Control Codes"
.ident "V00.00"

.data

.macro gen_term_data name string
    .global _TERM_\name
    _TERM_\name: .string string
.endm
gen_term_data HOME_CURSOR	"\x1b[H"
gen_term_data CLEAR_SCREEN	"\x1b[2J"
gen_term_data SAVE_CURSOR	"\x1b7"
gen_term_data RESTORE_CURSOR	"\x1b8"

.end                      

