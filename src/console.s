.title "con: system console routines"
.include "neoteric.s"

// CONSOLE (DU11 SYNCHRONOUS LINE INTERFACE)
.global con_putch
.global con_putcstr
.global con_putnstr
.global con_txrdy

RXCSR	    = 0177560	// receiver data buffer register
RXDBUF	    = 0177562	// receiver data buffer register, and also...
PARCSR	    = 0177562	// ...parameter control register
TXCSR	    = 0177564	// transmitter status register
TXDBUF	    = 0177566	// transmitter data buffer register

TXRDY_MASK  = 128	// mask to detect ready bit
NRETRY	    = 5000	// timeout retries

.text

/*
 * con_putch: send a character to the system console
 *    .text
 *         push	    $42	    // ascii asterix
 *         jsr	    pc, con_putch
 *         dparam   1
 */
con_putch:
	jsr	pc, con_txrdy
	paramb	0, TXDBUF
        rts     pc

/* con_putcstr: send a null-terminated string to the console
 *    .text
 *         push	    $hello  // string starting address
 *         jsr	    pc, con_putcstr
 *         dparam   1
 *    .data
 *    hello: .string  "Hello, world!!"
 */
con_putcstr:
	pushbp
	push	r0

	param	0	    // address of zero-terminated string
    0$:	tstb	(r0)	    // reached null byte?
	beq	1$	    // ...done
	pr_char	(r0)+	    // print it, advance pointer
        br	0$	    // for all characters

    1$:
	pop	r0
	popbp
        rts     pc

/* con_putnstr: send a counted string to the system console
 *    .text
 *         push	    $hello	// string starting address
 *         push	    $hello_len	// string character count
 *         jsr	    pc, con_putnstr
 *         dparam   2
 *    .data
 *    hello: .ascii  "Hello, world!!"
 *    hello_len = . - hello
 */
con_putnstr:
	pushbp
	push	r0
	push	r1
	param	0, r0		// string address
	param	1, r1		// loop counter

    0$:	pr_char	(r0)+
	sob	r1, 0$		// rest of characters

        pop	r1
        pop	r0
	popbp
        rts     pc

/* con_txrdy: wait for console tx status to become ready
 */
con_txrdy:
	push				// r0
	mov 	$NRETRY, r0		// initialize timeout countdown

    0$:	bit     $TXRDY_MASK, TXCSR	// ready for tx?
        bne     9$
        sob     r0, 0$			// countdown and retry
	// TODO: Handle timeout

    9$:	pop				// r0
        rts     pc

.end

