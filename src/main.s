.title	"NeotericOS"
.include "neoteric.s"

.global start
.global _system_shutdown


STACK_LOCATION = 01000
// Stack Limit Zones
// Yellow Zone: SL+340 to SL+377 -> execute, then trap
// Red Zone: SL+337 or below -> abort, then trap to location 4
STACK_LIMIT_REG = 0177774
STACK_LIMIT= 0


.text

start:
    preinit:
	// Set up the stack
        mov     $STACK_LOCATION, sp
	mov	$STACK_LIMIT, STACK_LIMIT_REG

	// Initialize register state
	mov	$0, r5		// base pointer

	clr	_system_shutdown

    init:
	jsr	pc, _sys_init

    run:
	wait

 	tst	_system_shutdown	    // system shutdown requested
 	bne	shutdown

	// TODO: Some way to exit loop
	jsr	pc, lineclock_get_update    // check if seconds have advanced
	tst	r1			    // no update?
	beq	run			    // back to wait loop
	jsr	pc, lineclock_clear_update  // updated? clear update flag bit
	pr_char	$42			    // print asterisk on console
	br	run			    // back to loop

    shutdown:
	pr_crlf
	pr_strl	$_shutdown_requested_msg
	jsr	pc, con_txrdy
	jsr	pc, lineclock_stop
	pr_strl	$_shutdown_complete_msg
	pr_crlf
	halt

.data

_system_shutdown: .word 0
_shutdown_requested_msg: .string "System shutting down."
_shutdown_complete_msg: .string "System halting."

.end start

